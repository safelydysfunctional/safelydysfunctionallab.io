let
  pinnedPkgs = import ./pkgs-from-json.nix { json = ./release-21.05.json; };
in
pinnedPkgs.pkgs.haskellPackages.callPackage ./blog.nix { }
