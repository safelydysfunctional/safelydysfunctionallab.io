{ mkDerivation, base, binary, bytestring, data-default, directory
, filepath, fsnotify, hakyll, http-types, lib, pandoc, pandoc-types
, process, tagsoup, text, wai, wai-app-static, wai-extra, warp
, pythonPackages
}:
mkDerivation {
  pname = "blog";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base binary bytestring data-default directory filepath fsnotify
    hakyll http-types pandoc pandoc-types process tagsoup text wai
    wai-app-static wai-extra warp
  ];
  executableSystemDepends = [ pythonPackages.pygments ];
  license = lib.licenses.gpl3;
  hydraPlatforms = lib.platforms.none;
}
