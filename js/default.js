(function () {
  const path = window.location.pathname;

  const links = document.getElementById("sidebar-menu")
        .getElementsByClassName("nav-link");

  for (let i = 0; i < links.length; i++) {
    if (path === links[i].pathname) {
      links[i].classList.add("active");
    }
  }
})();
