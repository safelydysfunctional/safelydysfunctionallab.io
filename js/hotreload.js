(function () {
  const content = document.getElementById("content");
  const partialLocation = window.location.pathname + ".partial";

  function tryConnect(retry, tries) {
    const source = new EventSource('/server-events');

    source.onmessage = function (ev) {
      console.log(ev);
    };

    source.addEventListener('update', async ev => {
      tries = 0;

      const body = await (await fetch(partialLocation)).text();

      content.innerHTML = body;
    });

    source.onerror = function () {
      if (tries < retry)
        tryConnect(retry, tries + 1);
    };
  }

  tryConnect(10, 0);
})();
