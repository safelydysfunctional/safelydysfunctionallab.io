module WatchServer where

import Data.String (fromString)

import Hakyll.Core.Configuration
import Hakyll.Core.Identifier
import Hakyll.Core.Identifier.Pattern
import Hakyll.Core.Runtime
import Hakyll.Core.Rules
import Hakyll.Core.Rules.Internal
import Hakyll.Core.Logger

import System.Directory (canonicalizePath)
import System.FilePath (pathSeparators)

import Control.Concurrent (forkIO)
import Control.Concurrent.MVar (newEmptyMVar, takeMVar, tryPutMVar)
import Control.Concurrent.Chan

import Control.Exception (AsyncException, handle, fromException, throw)

import Control.Monad (forever, void, when)

import Data.ByteString.Builder (byteString)

import qualified System.FSNotify as FSNotify

import qualified Network.Wai as Wai
import Network.Wai.EventSource
import qualified Network.Wai.Application.Static as Static
import qualified Network.Wai.Handler.Warp as Warp

import Network.HTTP.Types.Status (Status)

watchUpdates :: Configuration -> IO Pattern -> IO ()
watchUpdates conf update = do
    let providerDir = providerDirectory conf
    shouldBuild <- newEmptyMVar
    pattern         <- update
    fullProviderDir <- canonicalizePath $ providerDirectory conf
    manager         <- FSNotify.startManager

    let
      allowed event = do
        -- Absolute path of the changed file. This must be inside provider
        -- dir, since that's the only dir we're watching.
        let path = FSNotify.eventPath event
            relative   = dropWhile (`elem` pathSeparators) $
              drop (length fullProviderDir) path
            identifier = fromFilePath relative

        shouldIgnore <- shouldIgnoreFile conf path
        return $ not shouldIgnore && matches pattern identifier

    -- This thread continually watches the `shouldBuild` MVar and builds
    -- whenever a value is present.
    _ <- forkIO $ forever $ do
      event <- takeMVar shouldBuild
      handle
        (\e -> case fromException e of
                 Nothing    -> putStrLn (show e)
                 Just async -> throw (async :: AsyncException))
        (void update)

    -- Send an event whenever something occurs so that the thread described
    -- above will do a build.
    void $ FSNotify.watchTree manager providerDir (not . isRemove) $ \event -> do
        allowed' <- allowed event
        when allowed' $ void $ tryPutMVar shouldBuild event

watchHotReload :: Configuration -> Logger -> String -> Int -> Rules a -> IO ()
watchHotReload conf logger host port rules = do
  updateChan <- newChan

  -- _ <- forkIO $ forever $ readChan updateChan

  let
    dest = destinationDirectory conf
    watchApp req respond = do
      case Wai.pathInfo req of
        ["server-events"] -> do
          connChan <- dupChan updateChan
          eventSourceAppChan connChan req respond

        _ ->
          Static.staticApp (Static.defaultFileServerSettings dest) req respond

    update = do
      (_, ruleSet) <- run conf logger rules
      putStrLn "Sending Update"
      writeChan updateChan updateEvent
      return $ rulesPattern ruleSet

    warpSettings = Warp.setLogger noLog .
                   Warp.setHost (fromString host) .
                   Warp.setPort port $ Warp.defaultSettings

  _ <- forkIO $ watchUpdates conf update
  Warp.runSettings warpSettings $ watchApp
    
updateEvent :: ServerEvent
updateEvent = ServerEvent
  (Just . byteString $ "update")
  Nothing
  [byteString ""]

noLog :: Wai.Request -> Status -> Maybe Integer -> IO ()
noLog _ _ _ = pure ()

isRemove :: FSNotify.Event -> Bool
isRemove (FSNotify.Removed {}) = True
isRemove _                     = False
