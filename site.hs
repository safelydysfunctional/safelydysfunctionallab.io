--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import qualified Hakyll.Core.Configuration as Config
import Hakyll.Core.Logger as Logger
import Hakyll.Core.Rules
import Hakyll.Core.Runtime

import Data.Default (def)
import Data.Functor ((<&>))
import Text.Pandoc.Definition
import Text.Pandoc.Shared
import System.Process (readProcess)
import System.IO.Unsafe
import Data.Char (toLower)
import qualified Data.Text as T
import Data.Text (Text (..))

import qualified Text.HTML.TagSoup as TS

import Data.Traversable (traverse)

import WatchServer (watchHotReload)

import Data.Binary (Binary)
import Data.Typeable (Typeable)

import Control.Monad (when)

import System.Environment (getArgs)

import qualified Data.List as L

-- This piece does highlighting using pygmentize, which must be installed in the
-- (maybe the program should check for that in the beginning?). It is based on
-- https://gist.github.com/fizruk/6620756 but without unsafePerformIO (ew)

highlight :: Block -> Compiler Block
highlight block@(CodeBlock (_, "example":_, _) _) = return block
highlight (CodeBlock (_, options, _) code) = do
  pygmented <- pygments code options
  return . RawBlock (Format "html") $  pygmented
highlight block = return block

pygments :: Text -> [Text] -> Compiler Text
pygments code options
  | length options == 1 = fmap T.pack . unixFilter "pygmentize" ["-l", language, "-f", "html"] $ strCode
  | length options == 2 = fmap T.pack . unixFilter "pygmentize" ["-l", language, "-O linenos=inline", "-f", "html"] $ strCode
  | otherwise = return . T.concat $ ["<div class=\"highlight\"><pre>", code, "</pre></div>"]
  where
    language = T.unpack . T.toLower . head $ options
    strCode = T.unpack code

transformPandoc :: Pandoc -> Compiler Pandoc
transformPandoc (Pandoc meta blocks) = mapM highlight blocks <&> Pandoc meta

addHotReload :: Bool -> Item String -> Compiler (Item String)
addHotReload False = pure
addHotReload True = traverse (pure . withTagList addHotReload')
  where
    addHotReload' :: [TS.Tag String] -> [TS.Tag String]
    addHotReload' ts =
      let (prefix, suffix) = span (\case
                                      TS.TagClose "body" -> False
                                      _ -> True) ts
      in prefix ++ hrScript ++ suffix

    hrScript = [ TS.TagOpen "script" [("src", "/js/hotreload.js")]
               , TS.TagClose "script"]

partialBody :: Compiler (Item String)
partialBody = do
  id <- getResourceBody <&> itemIdentifier
  content <- loadSnapshot (setVersion Nothing id) "content"

  let item = Item { itemIdentifier = id, itemBody = itemBody content }

  loadAndApplyTemplate "templates/partial.html" defaultContext item

--------------------------------------------------------------------------------
rules :: Bool -> Rules ()
rules hotReload = do
  match "images/*" $ do
    route   idRoute
    compile copyFileCompiler

  match "css/*" $ do
    route   idRoute
    compile compressCssCompiler

  let jsPattern = if hotReload
                  then "js/*"
                  else "js/*" .&&. complement "js/hotreload.js"

  match jsPattern $ do
    route idRoute
    compile copyFileCompiler

  match (fromList ["about.html", "contact.org"]) $ do
    route   $ setExtension "html"
    compile $ pandocCompiler
      >>= saveSnapshot "content"
      >>= loadAndApplyTemplate "templates/default.html" defaultContext
      >>= relativizeUrls
      >>= addHotReload hotReload

  when hotReload . match (fromList ["about.html", "contact.org"])
    . version "partial" $ do
      route $ setExtension "html.partial"
      compile partialBody

  match "posts/*" $ do
    route $ setExtension "html"
    compile $ pandocCompilerWithTransformM def def transformPandoc
      >>= loadAndApplyTemplate "templates/post.html"    postCtx
      >>= saveSnapshot "content"
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= relativizeUrls
      >>= addHotReload hotReload

  when hotReload $ match "posts/*" $ version "partial" $ do
    route $ setExtension "html.partial"
    compile partialBody

  create ["archive.html"] $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll ("posts/*" .&&. hasNoVersion)
      let archiveCtx =
            listField "posts" postCtx (return posts) `mappend`
            constField "title" "Arquivo"            `mappend`
            defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
        >>= loadAndApplyTemplate "templates/default.html" archiveCtx
        >>= relativizeUrls

  create ["atom.xml"] $ do
    route idRoute
    compile $ do
      let feedCtx = postCtx `mappend`
            bodyField "description"

      posts <- fmap (take 10) . recentFirst =<< loadAllSnapshots ("posts/*" .&&. hasNoVersion) "content"

      renderAtom myFeedConfiguration feedCtx posts

  create ["rss.xml"] $ do
    route idRoute
    compile $ do
      let feedCtx = postCtx `mappend`
            constField "description" "This is the post description"

      posts <- fmap (take 10) . recentFirst =<< loadAll ("posts/*" .&&. hasNoVersion)

      renderRss myFeedConfiguration feedCtx posts

  match "index.html" $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll ("posts/*" .&&. hasNoVersion)
      let indexCtx =
            listField "posts" postCtx (return posts) `mappend`
            defaultContext

      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls

  match "templates/*" $ compile templateBodyCompiler

main = do
  args <- getArgs

  case args of
    ("hot-reload":_) -> hotReloadingServer
    [] -> hotReloadingServer
    _ -> hakyllWith (config False) $ rules False

  where
    hotReloadingServer = do
      logger <- Logger.new Logger.Message
  
      watchHotReload
        (config True)
        logger
        "localhost"
        9002
        (rules True)

--------------------------------------------------------------------------------
config :: Bool -> Configuration
config True = defaultConfiguration
config False = defaultConfiguration
               { destinationDirectory = "public"
               }

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
  { feedTitle = "Healthy  Cooking: latest recipes"
  , feedDescription = "This feed proivdes fresh recipes for fresh food!"
  , feedAuthorName = "John Doe"
  , feedAuthorEmail = "test@example.com"
  , feedRoot = "http://localhost:8000"
  }
