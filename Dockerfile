FROM nixos/nix

COPY . /app
WORKDIR /app
RUN nix-build default.nix
