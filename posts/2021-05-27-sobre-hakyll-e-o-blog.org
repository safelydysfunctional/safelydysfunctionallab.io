---
title: Sobre Hakyll e o Blog
modified: 2021-06-04
---

Okay, okay. Eu escrevi que usaria o LinkedIn, mas eu acabei mudando de ideia.
Não vou dar desculpas, o sistema de artigos deles é bem bacana, mas eu sou um
desenvolvedor de software, pelos deuses, se eu não puder optar pela caminho
programático quem pode?

Enfim, estando ciente que eu estava escrevendo um blog, decidi fazer o que
todas as crianças descoladas fazem hoje em dia: usar um Static Site Generator.

* Sobre Static Site Generators e Hakyll

Bom, se você nunca se deparou com esse tipo de ferramenta antes, pode estar se
perguntando: que merda é essa?

Tá no nome, cara (Static Site Generator - /gerador de sites estáticos/). É uma
ferramente que gera sites estáticos, como em "um site que você é apenas um
leitor passivo, sem capacidade de modificação". Sabe? Como eles faziam na
antiguidade, bem antes da peste. Em 1990.

Um SSG é um *gerador*. Acho que é óbvio então que ele vai gerar um site estático.
E por que um site estático, você pode me perguntar. Bom sites estáticos são
muito baratos para servir. Este aqui por exemplo está sendo servido
gratuitamente pelo [[https://docs.gitlab.com/ee/user/project/pages/][Gitlab Pages]]. Afinal de contas, são só uns arquivos HTML.

Outra parte importante de um SSG é que ele gera um site a partir de documentos
que não são necessariamente HTML. A maioria usa [[https://daringfireball.net/projects/markdown/][Markdown]], como [[https://jekyllrb.com/][Jekyll]] e [[https://gohugo.io/][Hugo]].
Então você escreve seus artigos em Markdown e o SSG se encarrega de transformar
o Markdown em HTML para você, aplicando os layouts e estilos da sua preferência.

Eu mesmo não sou um grande fã de Markdown. Meus favoritos são [[https://docutils.sourceforge.io/rst.html][ReST]] e
principalmente, acima de tudo [[https://orgmode.org/][Org]]. Veja bem, eu sou um usuário de [[https://www.gnu.org/software/emacs/][Emacs]] e
por consequência o org-mode é a única cosia que eu uso pra fazer anotações.

Por um outro lado, todo SSG exige que se escreva um pouco de código, e como eu
disse programação funcional é meu hobby. Eu queria um SSG que fosse configurável
através de uma linguagem funcional. Eu encontrei algumas coisas em Scala, mas
a ferramente mais completa que consegui encontrar foi o [[https://jaspervdj.be/hakyll/][Hakyll]] (não muito
criativo certo? Haskell + Jekyll, mas tudo bem, sigamos). Bom, da minha
perspectiva, isso é o que o Hakyll oferece:

- Escrito na mais funcional das linguagens de programação, Haskell;
- suporte para um número absurdo de linguagens de marcação, por que se integra
  ao [[https://pandoc.org/][Pandoc]], incluindo meu amorzinho, o org;
- bastante composicional, por causa de ser escrito em Haskell e tudo mais, eu
  acabei tomando vantagem disso nas primeiras 48 horas de uso;

Eu vou dizer que algumas alternativas (como [[https://hexo.io/][Hexo]]) tinham muitas outras vantagens
que me interessavam (como temas, por exemplo). Porém, quando descobri que Hakyll
suportava org, essa foi minha base de comparação. E advinha? Nenhum outra
suportava org tão bem quanto Hakyll e mais importante que isso, quase nenhum
outro era escrito em uma linguagem funcional.

Então Hakyll foi minha escolha.

** Melhorando Hakyll

Assim que escolhi usar o Hakyll eu percebi algumas funcionalidades que fariam
falta:

- Syntax highlighting de qualidade, preferencialmente com pygments;
- hot reload;
- usar os metadados do org ao invés que um cabeçalho separado.

*** Syntax Highlighting

Duas dessas eu alcancei. A primeira correção foi o syntax highlighting com
pygments, eu achei um Gist que criava um filtro de Pandoc transformar blocos
de código usando o pygments. Esse trecho tinha alguns problemas (como o uso
de ~unsafePerformIO~ para um chamada de processo), e eu acabei corrigindo e
adaptando para usar com o Hakyll. Esse é o trecho que faz isso:

#+NAME: site.hs
#+BEGIN_SRC haskell
highlight :: Block -> Compiler Block
highlight (CodeBlock (_, options, _) code) = do
  pygmented <- pygments code options
  return . RawBlock (Format "html") $  pygmented
highlight block = return block

-- Define a transformação de texto usando pygments. Opera dentro da monad
-- Compiler, definado pelo hakyll
pygments :: Text -> [Text] -> Compiler Text
pygments code options
  | length options == 1 = fmap T.pack . unixFilter "pygmentize" ["-l", language, "-f", "html"] $ strCode
  | length options == 2 = fmap T.pack . unixFilter "pygmentize" ["-l", language, "-O linenos=inline", "-f", "html"] $ strCode
  | otherwise = return . T.concat $ ["<div class=\"highlight\"><pre>", code, "</pre></div>"]
  where
    language = T.unpack . T.toLower . head $ options
    strCode = T.unpack code

transformPandoc :: Pandoc -> Compiler Pandoc
transformPandoc (Pandoc meta blocks) = mapM highlight blocks <&> Pandoc meta

rules hr = do
  -- ...
  match "posts/*" $ do
    route $ setExtension "html"

    -- Usa a função de transformação
    compile $ pandocCompilerWithTransformM def def transformPandoc
      >>= loadAndApplyTemplate "templates/post.html"    postCtx
      -- 
#+END_SRC

Simples, certo? A função ~unixFilter~ se encarrega de converter o texto do
CodeBlock em HTML usando o programa =pygmentize= (que precisa estar instalado).

*** Hot Reload

Agora, hot reload era mais um frescura. É simplemente legal poder ver suas
alterações acontecendo em "tempo real" no browser. Era um desafio também, eu
nunca tinha implementado um hot reloader antes.

Eu vou detalhar aqui a minha solução, mas não vou falar do código. É bastante
coisa, e se você tem interesse dá uma olhada no arquivo =src/WatchServer.hs=.

O primeiro passo pra fazer hot reload é a habilidade de detectar modificações
no sistema de arquivos. Essa já estava resolvida. O Hakyll já tem um comando
~watch~ que observa o sistema de arquivos e gera o site de novo quando há
mudanças. Pra detectar as mudanças no sistema de arquivos eu usei a
implementação deles.

O segundo passo é notificar o browser. Parece o caso de uso perfeito para
WebSockets, certo? *BEEEEH*, não! Eu escolhi notificar o browser usando
Server-Sent Events. SSE é um protocolo mais leve e a comunicação é em só uma
direção, exatamente o que eu precisava nesse caso. A implementação de SSE no
servidor também é bem mais fácil que WS.

Adicionalmente, estar usando Haskell tornou o processo de fazer broadcast de
eventos bem simples por causa de uma primitiva de concôrrencia chamada [[https://hackage.haskell.org/package/base-4.15.0.0/docs/Control-Concurrent-Chan.html][Chan]].
~Chan~'s são canais, onde você pode colocar valores (eventos no meu caso) e
as outras threads ficam esperando para ler.

Agora, uma vez que o eu pudesse enviar eventos para o browser, eu precisava de
uma maneira de receber esses eventos lá. Pra isso eu preciso de JavaScript que
eu só queria que estivesse na página caso o servidor estivesse em modo de
hot-reload.

A maneira mais simples que eu encontrei de adicionar coisas ao HTML gerado pela
engine do Hakyll foi carregar o HTML com Blaze e modificar as tags. E foi assim
que eu fiz.

Caso o servidor esteja em modo de hot reload o arquivo JS é copiado para a pasta
de destino e uma tag =<script>= é adicionada ao template.

O script é bem simples, usa a WebAPI [[https://developer.mozilla.org/en-US/docs/Web/API/EventSource][EventSource]] e a cada evento simplesmente
substitui a seção principal da página com uma renderização parcial do artigo.

Essa renderização parcial também só acontece no caso do servidor estar rodando
em modo hot-reload. Ela usa um resultado parcial da renderização completa das
páginas (chamada de /snapshots/ no linguajar do Hakyll) que armazena e depois
renderizada em outro arquivo com o nome no formato
/nome-do-artigo.html.partial/.

E é assim que eu faço o hot-reload.

* Conclusão

É isso. Essa foi minha trajetória pra conseguir um renderizador pro meu blog.

Existem algumas coisas que eu gostaria que fossem diferentes, como um bom
suporte a internacionalização, uma linguagem de marcação melhor para templates,
entre outras coisas, no entanto, esse setup é mais do que o suficiente por
enquanto. É natural que adicione esses detalhes conforme eu vá precisando de
mais funcionalidades para blog, e isso claramente não vai acontecer agora, no
primeiro post.

*PS*: Algumas coisas mudaram desde que comecei a escrever esse post (12 de maio)
para agora. Uma das coisas é que eu gostaria que o SSG fosse escrito em Scala,
cuja com a qual eu tenho mais experiência trabalhando e maior facilidade em
modificar, então é possível que eu venha a substitui a engine no futuro. Meu
único problema real continua senda as engines de template da Scala que são
meh no ponto de vista, mas essa também é uma correção necessário para o Hakyll,
então...

*EDIT 04/06*: Corrigi alguns erros ortográficos e percebi que não falei sobre o
Gitlab Pages, então o removi do título. Está vindo um artigo falando sobre
docker + hakyll + Gitlab Pages.
