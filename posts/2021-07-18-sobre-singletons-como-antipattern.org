---
title: Sobre Singletons como um anti-pattern
---

Esse post tem o potencial de ser mais um rant do que instrutivo. A semana
passada eu tive uma experiência horrível com singletons, e esse é post dedicado
a eles, aparentemente queridinhos dos programadores e que mesmo sendo tão mal
vistos ainda continuam sendo utilizados e ensinados amplamente.

* Minha história

Bom, então qual foi meu problema certo? Eu normalmente não utilizo singletons
no código que eu escrevo, especialmente porque eu utilizo linguagens que tem
construções mais adequadas para cobrir o caso de uso dos singletons e quando
eu me vejo numa situação onde um é necessário eu tendo a procurar uma outra
solução e refatorar o meu código para que eu não tenha que utilizar esse padrão
horrendo.

MAS, nunca trabalhamos sozinhos na nossa área certo? Um dos meus colegas havia
deixado uma biblioteca que fazia interface com um serviço web que nós usamos.
Esse serviço precisa ser acessado no background (pois os processos ao redor
dele são demorados para executar) para isso usamos Celery.

Bom a biblioteca usava um objeto de configuração para configurar o endereço e
autenticação do serviço. Além disso existem várias interfaces que usam essa
configuração internamente, cada uma especifica meio que um subsistema diferente
do serviço.

Pois bem, esse objeto de configuração foi definido como um singleton. Imagino
que o uso original desse singleton fosse para que todas as interfaces pudessem
usar a mesma configuração (já que são utilizadas em conjunto) e não usar muita
memória (usando uma configuração compartilhada), assim como evitar o chamada de
construtor pesado (que lia um arquivo de configuração do disco e o validava).

O objeto tinha a capacidade de notificar as interfaces de modificações que
haviam acontecido nela própria (era um Observable). O design claramente tinha
sido feito por alguém que manjava de patterns.

Porém, eu precisava usar essa biblioteca em um ambiente de alta concorrência
(em um worker usando gevent) e que se comunicava com servidores diferentes. E o
uso do singleton criou um problema tão profundo nesse cenário que eu tive que
refatorar a biblioteca.

Então vamos falar sobre esse monstro, shall we?

* Singleton

O que é o singleton, então? Um demônio que fica espreitando você no plano de
fundo da sua aplicação até que ele possa consumir sua alma. hehe

Não, mas sério, o singleton é um padrão para criação de objetos que só devem
ter/podem ter uma única instância. Eis aqui um exemplo de um singleton em Java:

#+BEGIN_SRC java
  public class BluetoothAdapter {
    private static BluetoothAdapter instance = null;

    public static BluetoothAdapter getInstance() {
      if (instance == null)
        instance = new BluetoothAdapter();

      return instance;
    }

    private BluetoothSystemHandler handler;
    private BluetoothMessage broadcastMessage;
    // Estado

    private BluetoothAdapter() {
      this.handler = getHandlerFromSystem();
    }

    public void broadcast(BluetoothMessage message) {
      this.broadcastMessage = message;
      // Faz o que tem que fazer
    }

    public boolean isActive() {
      return handler.isActive();
    }
  }
#+END_SRC

A necessidade do singleton aqui aparece porque a criação de um segunda
instância é ruim, cria um comportamento indesejado ou a instância é muito
pesada e criar múltiplas versões desta instância pode tem um impacto grande
no uso de memória.

Com um singleton todo estado fica contido dentro da classe, e você só pode
ter uma instância, garantida pelo implementação, então tudo na sua aplicação
que precisa de acessar esse código o faz através de um mesmo ponto de entrada.

E esse daqui é um exemplo de uso desse singleton:

#+BEGIN_SRC java
  void broadcastRegion() {
    var instance = BluetoothAdapter.getInstance();

    if (instance.isActive()) {
      instance.broadcast(this.region.toBluetoothBroadcast());
    }
  }
#+END_SRC

Esse é um uso genuíno do singleton, eu escolhi fazer esse exemplo em Java
porque em Java, o singleton faz sentido muitas vezes porque Java não tem
muitas outras maneiras de fazer uma coisa terrível: estado global.

* Estado global

OPA! Usei a expressão amaldiçoada: "estado global". Desde de pequeno a gente
ouve que ter estado global em uma aplicação é ruim, certo? E é mesmo, cria
diversos problemas, poluí o espaço de nomes, é completamente imprevisível. Mas
aí a gente vê um singleton e fala: "legal, um design pattern", e nem percebemos
que o singleton é estado global.

É estado global preguiçoso, de certa forma, mas não é nada além de estado global.

E eu vou dizer: em alguns casos, estado global é OK. O meu exemplo acima
poderia ser um desses casos. Não sei o quanto vocês conhecem de Bluetooth, mas
no Linux o Bluetooth é controlado por um serviço chamado =bluez= e esse
serviço é normalmente gerenciado através do =DBus=. Só existe uma instância do
=bluez= rodando no seu sistema em qualquer momento que seja, e essa instância
tem o controle sobre hardware. O controle sobre o hardware é um recurso
bastante limitado

Esse é um exemplo de estado ABSOLUTAMENTE global. Se eu crio uma classe
controladora para esse serviço na minha aplicação, ela pode ser global, porque
só existe um desses serviços em todo o meu sistema. Nestes casos, o singleton é
okay. É estado global se comunicando com um serviço que existe sozinho no
sistema, eu não preciso de mais de uma instância desse serviço e mesmo se eu
tivesse mais de uma outra instância, nada mudaria, porque eu ainda estaria
tentando controlar o mesmo sistema, com ambas as instâncias.

Um outro problema que singletons resolvem é dar unicidade pra objetos que são
muito pesados ou tem um processo de inicialização muito pesado. Um exemplo
disso poderia ser a configuração global da sua aplicação, ou sei lá, uma troca
de chaves com o servidor que envolve múltiplas etapas e depois é armazenada em
cache local.

Claro, tem encapsulamento, mas é um mesmo objeto compartilhado em todos os
lugares que essa classe aparece no seu código. O estado talvez não seja
publicamente compartilhado, mas é compartilhado implicitamente.

* Problemas

Se você acessa uma informação em um singleton, você está acessando uma
informação global, se você modifica essa informação, você está a modificando
globalmente. E isso carrega todos os perigos de estado global que você
normalmente esperaria, e seriamente afeta o design da sua aplicação,
especialmente quando você começa a lidar com concorrência ou tem uma base de
códigos muito grande.

** Imprevisibilidade

Vamos considerar um singleton mais perigoso, parecido com o que eu tive que
lidar no trabalho:

#+BEGIN_SRC java
  public class Configuration {
    private static Configuration instance = null;

    public Configuration getInstance() {
      if (instance == null)
        instance = new Configuration();
      return instance;
    }

    private String userName;
    private String token;

    private String authToken;

    private BaseRequest request;

    private Configuration() {
      this.state = new ConfigState();
      this.userName = null;
      this.token = null;
      updateConfig();
    }

    private void updateConfig() {
      this.authToken = "user=" + userName + "; token=" + token;
      this.baseRequest = new BaseRequest("https://my.service.com/",
          this.authToken());
    }

    public void setUserName(String userName) {
      this.userName = userName;
      updateConfig();
    }

    public void setToken(String token) {
      this.token = token;
      updateConfig();
    }

    public String getAuthToken() {
      return this.authToken;
    }

    public BaseRequest getBaseRequest() {
      return this.request;
    }
  }
#+END_SRC

Pois bem, agora eu faço isso:

#+BEGIN_SRC java
  void showConfign() {
    final var config = Configuration.getInstance();

    FileSystem.getFile("user.txt", config::setName);
    WebService.get("/token?app=ec819a0bdeef", config::setToken);

    System.out.println(config.getAuthToken());
  }
#+END_SRC

Onde ambos ~FileSystem.getFile~ e ~WebService.get~ são métodos assíncronos.
Qual resultado que vai ser exibido na tela? Quem sabe, certo?

Esse exemplo é simples, eu poderia só reordenar as chamadas para esperar a
configuração estar completa antes de chama o ~println~, mas você lembra que
o singleton é um estado global? Eu posso ter uma outra parte da aplicação,
completamente desconexa modificando essa configuração nesse exato momento.

E se alguma outra parte da aplicação, não relacionada, modificar esse objeto,
de repente eu tenho uma surpresa: como esse objeto foi modificado? Onde? O que
eu posso fazer para consertar?

A não ser que eu tenha as duas partes do sistema rodando na ordem certa, é capaz
de que eu nem seja capaz de replicar o bug. Em um ambiente de teste então, é
extremamente improvável que eu consigo detectar alguma coisa errada.

** Falta de testabilidade

Vamos supor que tenha uma classe assim:

#+BEGIN_SRC java
  public class ClientService {
    private Configuration config;

    public ClientService() {
      this.config = Configuration.getInstance();
    }

    public List<Client> getClients() {
      return this.config.getBaseRequest().get("/clients").executeForResponse();
    }
  }
#+END_SRC

Agora eu quero testar o meu ClientService, eu quero usar um mock nesse
BaseRequest pra ver se ele está indo para o lugar certo. Como eu faço?

Usando um singleton, você não faz. Essa classe *não pode* ser testada. É
impossível.

** Rigidez

Vamos supor agora que eu preciso de acessar também o serviço em
"=http://thirdparty.service.com=", um serviço relacionado, e eu queria usar esse
ClientService aí em cima, o que eu poderia fazer?

Repetir código, só isso. Não dá pra mudar a implementação de ~Configuration~.
Outros serviços dependem de ~Configuration~ como está. Não pra mudar a
implementação de ~ClientService~. Ele acessa os clientes para o primeiro
serviço.

Eu não posso mudar uma implementação de ~Configuration~ sem afetar TODAS as
partes do sistema que o usam.

E eu não sou o único que pensa que singletons são horrendos:

- [[https://www.michaelsafyan.com/tech/design/patterns/singleton]]
- [[http://misko.hevery.com/2008/08/17/singletons-are-pathological-liars/]]
- [[https://puredanger.github.io/tech.puredanger.com/2007/07/03/pattern-hate-singleton/]]

* Solução

E agora, eu acho que você pode estar se perguntando qual solução eu proponho,
certo?

É bem simples na verdade: não use singletons. Toda vez que você acha que
precisa de um singleton, tente resolver de alguma outra maneira. O nosso
exemplo de configuração por exemplo poderia ser resolvido dessa forma:

#+BEGIN_SRC java
  public class Configuration {
    private String authToken;
    private BaseRequest request;

    public Configuration(String userName, String token) {
      this.authToken = "user=" + userName + "; token=" + token;
      this.request = new BaseRequest("http://my.service.com", this.authToken);
    }

    public String getAuthToken() {
      return this.authToken;
    }

    public BaseRequest getBaseRequest() {
      return this.request;
    }
  }

  public class ClientService {
    private Configuration config;

    public ClientService(Configuration config) {
      this.config = config;
    }

    public List<Client> getClients() {
      return this.config.getBaseRequest().get("/clients").executeForResponse();
    }
  }
#+END_SRC

Não é perfeito, a configuração poderia ser mais customizável, mas a questão
aqui é: a configuração não é global, alterar uma instância da configuração não
muda na outra. É extremamente substituível e por consequência testável e
extensível. Preciso de uma mock ~BaseRequest~? Derivo ~Configuration~ e
sobrescrevo o método ~getBaseRequest~ para retornar o mock que eu quiser. Além
disso a configuração é imutável, então eu sei que para esse objeto os valores
de ~getAuthToken~ e ~getBaseRequest~ nunca vão mudar na execução do meu
programa, se eu preciso que eles mudem, eu preciso de ser EXPLÍCITO sobre
isso. Eu preciso de criar uma configuração nova, reinicializar os meus
serviços. O estado dos objetos configurados com ~Configuration~ não pode estar
inconsistente.

** Inversão de controle

Essa abordagem é chamada de inversão de controle, em essência eu tiro o
controle do ciclo de vida de dentro da classe e entrego ela nas mãos de quem
quer que seja que queria uma instância dessa classe.

Se eu tenho múltiplos serviços que dependem do mesmo objeto eu posso criar um
processo de inicialização dessa forma (dessa vez em JS, cansei de digitar
haha):

#+BEGIN_SRC js
  async function initializeApp() {
    const userName = await FS.getFile("user.txt");
    const token = await fetch("/token?app=my-js-app");

    const config = new Configuration(userName, token);

    startApplication({
      services: {
        clientService: new ClientService(config),
        orderService: new OrderService(config),
        kitchenQueue: new KitchenQueue(config)
      }
    });
  }
#+END_SRC

Esse é um exemplo de IoC (Inversion of Control) usando DI (Dependency
Injection, injeção de dependências). Outra maneira de fazer isso é com Service
Locator, que é um singleton que encontra as dependências pra você. Oras, qual
a vantagem então? Bom, o Service Locator é configurável, então, apesar de ser
um singleton, os objetos que ele retorna ("locate") não são. Vou deixar um
artigo do Martin Fowler aqui sobre Service Locator e IoC em geral:

[[https://martinfowler.com/articles/injection.html#UsingAServiceLocator]]

Eu já usei uma vez em projeto na universidade, é um conceito interessante e
funciona bem pra linguagens dinâmicas quando você tem muitas dependências.

Existem também frameworks que fazem DI automaticamente pra você. O poder da
framework varia muito de acordo com a linguagem/plataforma que você usa e elas
costumam deixar as coisa bem menos explícitas, mas elas funcionam e muito bem.
Em linguagens dinâmicas, elas são... complicadas. Porque é difícil de
especificar o que precisa ser injetado aonde. Nas estáticas é mais fácil porque
basta você combinar um objeto com um tipo.

Se você precisar de injetar muitas dependências, elas absolutamente valem a
pena. Se forem poucas injeções, a abordagem manual ali em cima costuma ser
melhor.

** Módulos e globais

Bom, ainda existem os casos onde singleton são necessários, porque você precisa
*um* de alguma coisa, e nunca mais do que um.

Um singleton é uma solução válida neste caso, porém temos alternativas.

Em Python (e JavaScript, em certos ambientes) nós temos módulos, o exemplo do
Bluetooth por exemplo poderia ser implementado dessa forma, em Python:

#+BEGIN_SRC python
handler = get_system_handler()
broadcast_message = None


def broadcast(message):
    global broadcast_message, handler

    broadcast_message = message
    # Faz o que tem que fazer


def is_active():
    return handler.is_active()
#+END_SRC

Em JavaScript:

#+BEGIN_SRC js
const handler = getSystemHandler();
let broadcastMessage = null;

export function broadcast(message) {
  broadcastMessage = message;
  // Faz o que tem que fazer
};

export function isActive() {
  return handler.isActive();
};
#+END_SRC

Uma grande parte das frameworks de desenvolvimento web de Python usam
configurações globais, como Django. É ruim, porque é bem mais difícil de isolar
as coisas, mas Django tem uma boa desculpa: a configuração é a mesma pra
aplicação inteira. Se você precisa de outras configurações, você precisa de
outras aplicações. 

Você pode estar pensando: mas pera aí, você tá defendendo o uso de estado
global? E é isso mesmo, eu estou defendendo o uso de estado global EXPLÍCITO.
Alguém usando um desses módulos vai ser saber que estão alterando estado global
e você desenvolvendo o módulo global, vai saber que está criando código
dependente de estado global. Se você acha que estado global é uma ideia ruim
para o seu código, você vai refatorar, se não, é porque você realmente está
lidando com um dos raríssimos casos onde estado global é necessário.

Outro detalhe é que linguagens dinâmicas como Python e JavaScript suportam o
conceito de Monkey Patching, que consiste em substituir a definição de nomes
globais por outros objetos em tempo de execução. Tornando isso aí testável,
mesmo sendo estado global.

Outra estratégia para criação de módulos "globais" são os objetos de linguagens
como Scala e Kotlin:

#+BEGIN_SRC scala
  object BluetoothAdapter {
    private lazy val handler = getSystemHandler()
    private var broadcastMessage: Option[BluetoothMessage] = None

    def broadcast(message: BluetoothMessage): Unit = {
      broadcastMessage = message
     // Faz o que tem que fazer
    }

    def isActive(): Boolean = handler.isActive();
  }
#+END_SRC

Honestamente, se você ver aquela ~var~ ali em Scala e não achar que tem alguma
coisa errada, você provavelmente está sem nenhuma outra saída. E esse objeto
é explicitamente global e único, por definição sintática da linguagem. Não tem
espaço pra surpresas aqui. É um singleton, claro, mas é explicitamente um
singleton. E você vai tomar muito cuidado quando se deparar com um monstro
desses.

E é essa é minha proposta: torne estado global compartilhado alguma coisa
explicita. Nós, como desenvolvedores, temos uma aversão intrínsica a estado global
que forçada é na nossa consciência durante nossa educação. Se a gente olhar pra
um código que faz coisas globais e ainda acharmos que é uma boa ideia, não é,
mas provavelmente é nossa única saída.

E o singleton é isso, estado global, mas por estar encapsulado em uma
abstração bonitinha a gente começa a tratar como se não fosse. E é aí que
mora o perigo, e aí que o singleton pega a gente de surpresa.

* Conclusão
  
Eu ainda não encontrei uma só situação onde singleton fosse *necessário*
(apesar de ter encontrado várias situações onde fosse /conveniente/). E nenhuma
situação real onde ele fosse uma boa decisão de design.

Quando pensar em criar um singleton, minha sugestão é procurar uma solução
melhor, tentar modificar o design para que ele não seja necessário,
provavelmente o problema que você está tentando resolver só existe porque
alguma parte do seu design é ruim.

E, se for pra usar singleton, pode usar, mas não o faça sem IoC. Não chame o
~getInstance~ diretamente dentro da sua classe, ao invés disso, crie o objeto
externamente e só então passe para o objeto sendo construído. Isso por si só
resolve uma parte dos problemas associados com singletons.

É isso dessa vez. Espero que tenha algum valor pra vocês e até a próxima. :)
